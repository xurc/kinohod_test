#!/bin/bash

docker-compose run --rm check_db && \
docker-compose up app && \
docker-compose stop db
