CREATE USER kinohod_test WITH PASSWORD 'kinohod';

CREATE DATABASE kinohod_test
    WITH
    OWNER = kinohod_test
    ENCODING = 'UTF8'
    LC_COLLATE = 'ru_RU.UTF-8'
    LC_CTYPE = 'ru_RU.UTF-8'
    TEMPLATE = template0;
