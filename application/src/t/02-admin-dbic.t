use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('KinohodTest');

# Просмотр схемы ( /admin/dbic )
$t->get_ok('/admin/dbic')->status_is(200)->content_like(qr/DBIC::Admin/i);

done_testing();
