use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $hdrs = { 'Accept' => 'application/vnd.api+json', 'Content-Type' => 'application/vnd.api+json', };

my $headers_ok = sub {
  my $t = shift;

  local $Test::Builder::Level = $Test::Builder::Level + 1;

  return $t->success(
    is( $t->tx->res->headers->content_type, 'application/vnd.api+json', "Response Content-type header" ) );
};

my $t = Test::Mojo->new('KinohodTest');

# API

# Create
my $place = {
              data => {
                type => "places",
                attributes => {
                  name => "Test Place"
                }
              }
            };

$t->post_ok( '/api/v1/admin/places' => $hdrs => json => $place )->status_is(200)->$headers_ok->json_has('/id');
my $place_id = $t->tx->res->json('/id');

$t->post_ok( '/api/v1/admin/places' => $hdrs => json => $place )->status_is(409);

# Read
$t->get_ok( "/api/v1/admin/places/$place_id", $hdrs )->status_is(200)
  ->$headers_ok->json_is( '/attributes/name' => 'Test Place' );

$t->get_ok( "/api/v1/admin/places/0", $hdrs )->status_is(404)->$headers_ok->json_has('/errors')
  ->json_is( '/errors/0/status' => 404 )->json_is( '/errors/0/title' => 'Not Found' );

# Update
$t->patch_ok( "/api/v1/admin/places/$place_id" => $hdrs => json =>
    { data => { id => $place_id, type => "places", attributes => { name => "Test Place Changed" } } } )->status_is(200)
  ->$headers_ok->json_is( '/attributes/name' => 'Test Place Changed' );

# List
$t->get_ok( '/api/v1/admin/places', $hdrs )->status_is(200)->$headers_ok->json_has('/data');

# Delete
$t->delete_ok( "/api/v1/admin/places/$place_id", $hdrs )->status_is(204);

done_testing();
