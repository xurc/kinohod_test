-- 1 up
CREATE TABLE places
(
    id     serial  PRIMARY KEY,
    name   text    NOT NULL UNIQUE,
    active boolean DEFAULT TRUE
);

CREATE INDEX places_active_idx ON places(active);

CREATE TABLE events
(
    id       serial       PRIMARY KEY,
    place    int          NOT NULL REFERENCES places(id) ON UPDATE CASCADE ON DELETE CASCADE,
    name     text         NOT NULL,
    start    timestamp    NOT NULL,
    duration interval     CHECK (duration > '0'),
    price    numeric(9,2) CHECK (price > 0),
    active   boolean      DEFAULT TRUE
);

CREATE INDEX events_place_idx  ON events(place);
CREATE INDEX events_active_idx ON events(active);
CREATE INDEX events_start_idx  ON events(start);

CREATE INDEX events_start_day_idx   ON events(date_trunc('day', start));
CREATE INDEX events_start_month_idx ON events(date_trunc('month', start));
CREATE INDEX events_start_year_idx  ON events(date_trunc('year', start));

CREATE TYPE order_state AS ENUM ('ACTIVE', 'CANCELLED', 'COMPLETED');

CREATE TABLE orders
(
    id      serial      PRIMARY KEY,
    event   int         NOT NULL REFERENCES events(id) ON UPDATE CASCADE ON DELETE CASCADE,
    state   order_state NOT NULL DEFAULT 'ACTIVE',
    tickets smallint    CHECK (tickets BETWEEN 1 AND 6)
);

CREATE INDEX orders_state_idx ON orders(state);
CREATE INDEX orders_event_idx ON orders(event);

-- 1 down
DROP TABLE orders;
DROP TABLE events;
DROP TABLE places;

DROP TYPE order_state;
