package KinohodTest::Command::migrations;

use Mojo::Base 'Mojolicious::Command';

sub run
{
  my ( $self, @args ) = @_;

  my $m = $self->app->pg->migrations->from_file( $self->app->home->rel_file('sql/migrations.sql')->to_string );

  $m->migrate             if $args[0] eq 'deploy';
  $m->migrate(0)->migrate if $args[0] eq 'reset';
  $m->migrate( $args[0] ) if $args[0] =~ /^\d+$/;
  say $m->active          if $args[0] eq 'active';
  say $m->latest          if $args[0] eq 'latest';

  exit;
}

1;
