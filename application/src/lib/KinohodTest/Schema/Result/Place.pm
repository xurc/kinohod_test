use utf8;
package KinohodTest::Schema::Result::Place;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

KinohodTest::Schema::Result::Place

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<places>

=cut

__PACKAGE__->table("places");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'places_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 active

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "places_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "active",
  { data_type => "boolean", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<places_name_key>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("places_name_key", ["name"]);

=head1 RELATIONS

=head2 events

Type: has_many

Related object: L<KinohodTest::Schema::Result::Event>

=cut

__PACKAGE__->has_many(
  "events",
  "KinohodTest::Schema::Result::Event",
  { "foreign.place" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-09-19 21:56:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4jjDW1KE/mo13Y7HEaLGhw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
