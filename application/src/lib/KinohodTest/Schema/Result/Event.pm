use utf8;
package KinohodTest::Schema::Result::Event;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

KinohodTest::Schema::Result::Event

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<events>

=cut

__PACKAGE__->table("events");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'events_id_seq'

=head2 place

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 start

  data_type: 'timestamp'
  is_nullable: 0

=head2 duration

  data_type: 'interval'
  is_nullable: 1

=head2 price

  data_type: 'numeric'
  is_nullable: 1
  size: [9,2]

=head2 active

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "events_id_seq",
  },
  "place",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
  "start",
  { data_type => "timestamp", is_nullable => 0 },
  "duration",
  { data_type => "interval", is_nullable => 1 },
  "price",
  { data_type => "numeric", is_nullable => 1, size => [9, 2] },
  "active",
  { data_type => "boolean", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 orders

Type: has_many

Related object: L<KinohodTest::Schema::Result::Order>

=cut

__PACKAGE__->has_many(
  "orders",
  "KinohodTest::Schema::Result::Order",
  { "foreign.event" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 place

Type: belongs_to

Related object: L<KinohodTest::Schema::Result::Place>

=cut

__PACKAGE__->belongs_to(
  "place",
  "KinohodTest::Schema::Result::Place",
  { id => "place" },
  { is_deferrable => 0, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-09-19 21:56:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:vAnp/m7A/fpRj4QneSuveQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
