use utf8;
package KinohodTest::Schema::Result::Order;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

KinohodTest::Schema::Result::Order

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<orders>

=cut

__PACKAGE__->table("orders");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'orders_id_seq'

=head2 event

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 state

  data_type: 'enum'
  default_value: 'ACTIVE'
  extra: {custom_type_name => "order_state",list => ["ACTIVE","CANCELLED","COMPLETED"]}
  is_nullable: 0

=head2 tickets

  data_type: 'smallint'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "orders_id_seq",
  },
  "event",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "state",
  {
    data_type => "enum",
    default_value => "ACTIVE",
    extra => {
      custom_type_name => "order_state",
      list => ["ACTIVE", "CANCELLED", "COMPLETED"],
    },
    is_nullable => 0,
  },
  "tickets",
  { data_type => "smallint", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 event

Type: belongs_to

Related object: L<KinohodTest::Schema::Result::Event>

=cut

__PACKAGE__->belongs_to(
  "event",
  "KinohodTest::Schema::Result::Event",
  { id => "event" },
  { is_deferrable => 0, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-09-19 21:56:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ooZRnUXGJyXvu90pUIl/Qw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
