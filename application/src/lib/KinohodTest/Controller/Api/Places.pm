package KinohodTest::Controller::Api::Places;

use Mojo::Base 'KinohodTest::Controller::Base';

has entity => 'Place';

=desription
    Здесь может быть внедрена дополнительная валидация запросов,
    имеющая отношение только к этой сущности, отличная от стандартной логика
    и т.д.

    Пример есть в KinohodTest::Controller::Api::Orders.
=cut

sub fetch_places         { shift->_fetch; }
sub get_place            { shift->_get; }
sub post_place           { shift->_post; }
sub patch_place          { shift->_patch; }
sub delete_place         { shift->_delete; }
sub get_related_events   { shift->_get_related('Event'); }
sub post_related_events  { shift->_post_related('Event'); }
sub patch_related_events { shift->_patch_related('Event'); }

1;
