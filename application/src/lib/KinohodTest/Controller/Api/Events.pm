package KinohodTest::Controller::Api::Events;

use Mojo::Base 'KinohodTest::Controller::Base';

has entity => 'Event';

=desription
    Здесь может быть внедрена дополнительная валидация запросов,
    имеющая отношение только к этой сущности, отличная от стандартной логика
    и т.д.
    
    Пример есть в KinohodTest::Controller::Api::Orders.
=cut

sub fetch_events         { shift->_fetch; }
sub get_event            { shift->_get; }
sub post_event           { shift->_post; }
sub patch_event          { shift->_patch; }
sub delete_event         { shift->_delete; }
sub get_related_orders   { shift->_get_related('Order'); }
sub post_related_orders  { shift->_post_related('Order'); }
sub patch_related_orders { shift->_patch_related('Order'); }

1;
