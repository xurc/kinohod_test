package KinohodTest::Controller::Api::Statistics;

use Mojo::Base 'Mojolicious::Controller';

=desription
  
=cut

sub get
{
  my $c = shift;

  $c->render( json => $c->statistics->get_orders_summary(
                                          from   => $c->param('from'),
                                          to     => $c->param('to'),
                                          period => $c->param('period'),
                                      )
  );
}

1;
