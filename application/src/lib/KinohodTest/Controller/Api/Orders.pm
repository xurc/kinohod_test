package KinohodTest::Controller::Api::Orders;

use Mojo::Base 'KinohodTest::Controller::Base';

has entity => 'Order';

=desription
    Здесь может быть внедрена дополнительная валидация запросов,
    имеющая отношение только к этой сущности, отличная от стандартной логика
    и т.д.
    
    В качестве примера, DELETE от пользователя не удаляет заказ, а проставляет статус отмены.
=cut

sub get_order    { shift->_get; }
sub post_order   { shift->_post; }
sub patch_order  { shift->_patch; }
sub delete_order { shift->_delete; }

sub cancel_order
{
  my $c = shift;

  my $entity = $c->schema->resultset( $c->entity )->find( $c->_get_url_id );
  return $c->render_error( 404, 'Not Found' ) unless $entity;

  $entity->update( { state => 'CANCELLED', } );
  $c->render( json => $c->resource_document($entity) );
}

1;
