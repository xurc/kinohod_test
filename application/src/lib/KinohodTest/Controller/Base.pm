package KinohodTest::Controller::Base;

use Mojo::Base 'Mojolicious::Controller';

has 'entity';

sub _get_url_id
{
  my $c = shift;
  return $c->param( lc( $c->entity . '_id' ) );
}

sub _fetch
{
  my $c = shift;

  my $res = $c->resource_documents(
                    $c->schema->resultset($c->entity),
                    {
                      includes => 'all_related',
                      with_attributes => 1,
                    },
  );
  
  $c->render( json => $res );
}

sub _get
{
  my $c = shift;

  my $entity = $c->schema->resultset( $c->entity )->find( $c->_get_url_id );
  return $c->render_error( 404, 'Not Found' ) unless $entity;

  my $res = $c->resource_document(
                    $entity,
                    {
                      with_attributes => 1,
                      includes => $c->stash->{includes}
                    },
  );
  
  $c->render( json => $res );
}

sub _get_related
{
  my ($c, $rel_name) = @_;

  my $entity = $c->schema->resultset($rel_name)->search( { $c->entity => $c->_get_url_id } );
  return $c->render_error( 404, 'Not Found' ) unless $entity;
  
  $c->render( json => $c->resource_documents($entity) );
}

sub _post_related
{
  my ( $c, $rel_name ) = @_;

  my $json = $c->stash->{json};
  return $c->render_error( 400, 'Bad Request' ) unless $json;

  $json->{data}->{attributes}->{ lc( $c->entity ) } = $c->_get_url_id;

  my $entity;

  eval { $entity = $c->schema->resultset($rel_name)->create( $json->{data}->{attributes} ); };
  return $c->render_error( 409, 'Conflict' ) if $@;

  $c->render( json => $c->resource_document($entity) );
}

sub _delete
{
  my $c = shift;

  my $entity = $c->schema->resultset( $c->entity )->find( $c->_get_url_id );
  return $c->render_error( 404, 'Not Found' ) unless $entity;

  $entity->delete;
  
  $c->rendered(204);
}

sub _post
{
  my $c = shift;

  my $json = $c->stash->{json};
  return $c->render_error( 400, 'Bad Request' ) unless $json;

  my $entity;

  eval { $entity = $c->schema->resultset( $c->entity )->create( $json->{data}->{attributes} ); };
  return $c->render_error( 409, 'Conflict' ) if $@;

  $c->render( json => $c->resource_document($entity) );
}

sub _patch
{
  my $c = shift;

  my $json = $c->stash->{json};
  return $c->render_error( 400, 'Bad Request' ) unless $json;

  my $entity = $c->schema->resultset( $c->entity )->find( $c->_get_url_id );
  return $c->render_error( 404, 'Not Found' ) unless $entity;

  eval { $entity->update( $json->{data}->{attributes} ); };
  return $c->render_error( 409, 'Conflict' ) if $@;

  $c->render( json => $c->resource_document($entity) );
}


sub _patch_related
{
  my ( $c, $rel_name ) = @_;

  my $json = $c->stash->{json};

  return $c->render_error( 400, 'Bad Request' ) unless $json;
  return $c->render_error( 400, 'Bad Request' ) unless $json->{data}->{id};

  my $entity = $c->schema->resultset($rel_name)->find( $json->{data}->{id} );
  return $c->render_error( 404, 'Not Found' ) unless $entity;

  eval { $entity->update( $json->{data}->{attributes} ); };
  return $c->render_error( 409, 'Conflict' ) if $@;

  $c->render( json => $c->resource_document($entity) );
}


1;