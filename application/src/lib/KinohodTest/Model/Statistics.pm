package KinohodTest::Model::Statistics;

use Mojo::Base -base;

has 'pg';

sub get_orders_summary
{
  my ($self, %params) = @_;

  my @binds;
  my $period = $params{period} // 'day';
  
  my $query = <<"SQL";
    WITH completed_orders AS (
      SELECT
          e.start,
          e.price,
          o.tickets,
          e.price * o.tickets AS cost
        FROM 
          events e
          JOIN orders o ON (o.event = e.id)
    --    WHERE
    --      o.state = 'COMPLETED'
    )
    SELECT 
        date_trunc( ?, start ) AS period,
        count(*)               AS count,
        sum(tickets)           AS tickets,
        sum(cost)              AS cost,
        avg(cost)              AS avg_cost
      FROM
        completed_orders
      WHERE TRUE
        @{[ $params{from} && $params{to} ? 'AND start BETWEEN ?::timestamp AND ?::timestamp' : '' ]}
      GROUP BY 
        period  
SQL
  
  push @binds, $period;
  
  if ($params{from} && $params{to})
  {
    push @binds, $params{from};
    push @binds, $params{to};
  }
  
  return $self->pg->db->query( $query, @binds )->expand->hashes;
}

1;

