package KinohodTest::Model::Orders;

use Mojo::Base -base;

has 'pg';
has 'log';

sub set_completed
{
  my $self = shift;
  
  my $query = <<"SQL";
  UPDATE
        orders AS o
    SET
        state = 'COMPLETED'
    FROM 
      events AS e
    WHERE
        o.event = e.id
        AND o.state = 'ACTIVE'
        AND e.start + e.duration < now()
SQL

  $self->log->debug('Running orders check');
  $self->pg->db->query_p($query)->wait;
}

1;