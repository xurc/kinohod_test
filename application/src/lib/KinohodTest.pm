package KinohodTest;

use Mojo::Base 'Mojolicious';
use Mojo::JSON qw(decode_json);
use Mojo::Pg;

use KinohodTest::Model::Statistics;
use KinohodTest::Model::Orders;

sub startup
{
  my $self = shift;

  $self->mode('production') unless $self->mode;
  
  # Чтение конфига
  my $config = $self->plugin('Config');

  # Подключение к БД
  $self->helper( pg => sub { state $pg = Mojo::Pg->new( $config->{database} ); } );
  
  # Модель
  $self->helper( statistics => sub { state $stat = KinohodTest::Model::Statistics->new( pg => $self->pg ) } );
  $self->helper( orders => sub { state $stat = KinohodTest::Model::Orders->new( pg => $self->pg, log => $self->log ) } );

  # Создаём схему БД, если нет
  my $m = $self->pg->migrations->from_file( $self->home->rel_file('sql/migrations.sql')->to_string );
  $m->migrate unless $m->active;

  # Фоновые задачи
  $self->plugin(
    Cron => (
      check_orders => {
        crontab => $config->{cron}->{set_orders_complete},
        code    => sub { $self->orders->set_completed; }
      }
    )
  ) if $config->{cron}->{enabled};
  
  # Команды для управления схемой
  push @{ $self->commands->namespaces }, 'KinohodTest::Command';

  # Роуты
  my $r = $self->routes;

  # Генератор роутов для JSON:API
  $self->plugin('JSONAPI', {namespace => 'api/v1'} );

  # API
  # В рамках тестового не делал схем авторизации и пользователей с ролями.
  # Генерацию роутов лучше сделать на основе файла спецификации
  my $api = $r->under(
    '/api/v1' => sub {
      my $c = shift;

      if ($c->req->headers->header('Accept') eq 'application/vnd.api+json')
      {
        my $includes = $c->requested_resources() // [];
        my $fields   = $c->requested_fields()    // {};

        $c->stash( includes => $includes );
        $c->stash( fields   => $fields );
        $c->stash( json => decode_json($c->req->body) ) if $c->req->body;

        return 1;
      }

      $c->render_error( 406, 'Not Acceptable' );
      return undef;
    }
  );

  # Администраторы
  my $admins = $api->any('/admin');
  $self->resource_routes( { router => $admins, resource => 'place', relationships => ['events'], } );

  # Пользователи
  my $user = $api->any('/user');

  # Пользователи: получение площадок и событий
  $user->get('/places')->to('api-places#fetch_places');
  $user->get('/places/:place_id')->to('api-places#get_place');
  $user->get('/places/:place_id/relationships/events')->to('api-places#get_related_events');
  $user->get('/places/:place_id/events')->to('api-places#get_related_events');
  
  # Пользователи: создание заказа
  $user->post('/places/:place_id/events/:event_id/orders')->to('api-events#post_related_orders');

  # Пользователи: отмена заказа
  $user->delete('/places/:place_id/events/:event_id/orders/:order_id')->to('api-orders#cancel_order');

  # Саппорт
  my $support = $api->any('/support');
  
  # Саппорт: получение заказа
  $support->get('/orders/:order_id')->to('api-orders#get_order');
  
  # Саппорт: изменение заказа
  $support->patch('/orders/:order_id')->to('api-orders#patch_order');

  # Саппорт: статистика
  $support->get('/stat')->to('api-statistics#get');
 
  # Просмотр схемы ( /admin/dbic )
  $r->add_condition( admin_dbic => sub { 1 } );

  $self->plugin('DBIC');
  $self->plugin( 'DBICAdmin' => { condition => 'admin_dbic', } );
  
  # Редирект с /
  $r->get( '/' => sub { shift->redirect_to('/admin/dbic') } );
}

1;
