# Тестовое задание для kinohod

```
# Запуск
./run.sh
```
Не успел за три вечера (зато, возможно, будет что обсудить):
  - фильтры по названиям площадок, событий, по датам и т.д.
  - валидацию
  - пользователей, роли, авторизацию
  - полную реализацию {json:api}
  - перенести логику в модель
  - нормально откомментировать код
  - полные тесты
  - параметры Dockerfile через переменные окружения (например, порты БД и приложения)

Сейчас приложение представляется из себя костяк, который в теории можно было бы развивать и дорабатывать дальше.

Фоновая смена статусов сделана на основе асинхронного неблокирующего кода, с которым Starman не умеет работать, как выяснилось под конец, поэтому запуск через hypnotoad, который умеет, но в теории этот функционал можно вынести в crontab и запускать приложение через starman, этот вариант запуска закомментирован в Dockerfile приложения. Если сейчас запустить приложение через Starman, оно будет работать, но не будет проставляться статус завершения заказа.

Есть минимум тестов (запускать при запущенных docker-compose):
```
# Тесты
./test.sh
```
Просмотр БД: http://localhost:5000/

Структура:
```
docker приложения ├── application
                  │   ├── cpanfile
                  │   ├── Dockerfile
                  │   └── src
Конфиг прода      │       ├── kinohod_test.conf
Конфиг отладки    │       ├── kinohod_test.development.conf
                  │       ├── kinohod_test.production.conf -> kinohod_test.conf
                  │       ├── lib
                  │       │   ├── KinohodTest
Команды           │       │   │   ├── Command
                  │       │   │   │   └── migrations.pm
Контроллеры       │       │   │   ├── Controller
                  │       │   │   │   ├── Api
                  │       │   │   │   │   ├── Events.pm
                  │       │   │   │   │   ├── Orders.pm
                  │       │   │   │   │   ├── Places.pm
                  │       │   │   │   │   └── Statistics.pm
                  │       │   │   │   └── Base.pm
Зачатки модели    │       │   │   ├── Model
                  │       │   │   │   ├── Orders.pm
                  │       │   │   │   └── Statistics.pm
DBIx::Class       │       │   │   ├── Schema
                  │       │   │   │   └── Result
                  │       │   │   │       ├── Event.pm
                  │       │   │   │       ├── Order.pm
                  │       │   │   │       └── Place.pm
                  │       │   │   └── Schema.pm
Приложение mojo   │       │   └── KinohodTest.pm
                  │       ├── log
                  │       ├── public
                  │       ├── script
                  │       │   └── kinohod_test
                  │       ├── sql
Схема БД          │       │   └── migrations.sql
Зачатки тестов    │       ├── t
                  │       │   ├── 01-basic.t
                  │       │   ├── 02-admin-dbic.t
                  │       │   └── 03-api.t
                  │       ├── templates
Запуск тестов     │       └── test.sh
docker-compose    ├── docker-compose.yml
docker PgSQL      ├── PostgreSQL
                  │   ├── docker-entrypoint-initdb.d
                  │   │   └── createdb.sql
                  │   └── Dockerfile
                  ├── README.md
Запуск            ├── run.sh
Внешний тест      └── test.sh
```

Все роуты для API в приложении:
```
/api/v1                                                   *  
  +/admin                                                 * 
    +/places                                              *
      +/                                                  GET
      +/                                                  POST
      +/:place_id                                         GET   
      +/:place_id                                         PATCH 
      +/:place_id                                         DELETE 
      +/:place_id/relationships/events                    GET     
      +/:place_id/events                                  GET     
      +/:place_id/relationships/events                    POST    
      +/:place_id/events                                  POST    
      +/:place_id/relationships/events                    PATCH   
      +/:place_id/events                                  PATCH   
      +/:place_id/relationships/events                    DELETE  
      +/:place_id/events                                  DELETE  
  +/user                                                  *       
    +/places                                              GET     
    +/places/:place_id                                    GET     
    +/places/:place_id/relationships/events               GET     
    +/places/:place_id/events                             GET     
    +/places/:place_id/events/:event_id/orders            POST    
    +/places/:place_id/events/:event_id/orders/:order_id  DELETE  
  +/support                                               *       
    +/orders/:order_id                                    GET     
    +/orders/:order_id                                    PATCH   
    +/stat                                                GET     
```

Примеры запросов к API (в некоторых нужно учитывать id объектов):
```
# Создать площадку
curl -X POST -H 'Accept: application/vnd.api+json' -H 'Content-Type: application/vnd.api+json' -i http://localhost:5000/api/v1/admin/places --data '{
  "data": {
    "type": "places",
    "attributes": {
      "name": "Curl Test Place"
    }
  }
}'

# Список площадок
curl -X GET -H 'Accept: application/vnd.api+json' -H 'Content-Type: application/vnd.api+json' -i http://localhost:5000/api/v1/admin/places

# Площадка по ID с событиями
curl -X GET -H 'Accept: application/vnd.api+json' -H 'Content-Type: application/vnd.api+json' -i 'http://localhost:5000/api/v1/admin/places/3?include=events'

# Изменение площадки
curl -X PATCH -H 'Accept: application/vnd.api+json' -H 'Content-Type: application/vnd.api+json' -i http://localhost:5000/api/v1/admin/places/12 --data '{
  "data": {
    "type": "places",
    "id": "12",
    "attributes": {
      "name": "Test Place 12"
    }
  }
}'

# Удаление площадки
curl -X DELETE -H 'Accept: application/vnd.api+json' -H 'Content-Type: application/vnd.api+json' -i http://localhost:5000/api/v1/admin/places/9
```
